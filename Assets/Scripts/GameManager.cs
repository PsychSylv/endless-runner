using EndlessRunner.Menu.Start;
using EndlessRunner.Player.Controller;
using EndlessRunner.Terrain.Pool;
using EndlessRunner.UI.Popup.GameOver;
using EndlessRunner.UI.Popup.Pause;
using EndlessRunner.Game.State;
using System;
using UnityEngine;

namespace EndlessRunner.Manager
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField] private StartMenuController startMenuController;
        [SerializeField] private PausePopupController pausePopupController;
        [SerializeField] private GameOverPopupController gameOverPopupController;
        [SerializeField] private PlayerController playerController;
        [SerializeField] private TerrainPoolController terrainPoolController;

        private GameState gameState;
        private const int MaxHealth = 3;
        private int health;
        private int coin;

        public event Action HealthReseted;
        public event Action<int> HealthUpdated;
        public event Action<int> CoinUpdated;
        public event Action<float> DistanceUpdated;
        public event Action CoinHitPlayed;
        public event Action ObstacleHitPlayed;
        public event Action PausePlayed;
        public event Action GameOverPlayed;

        private void Start()
        {
            startMenuController.StartPressed += OnStartGame;
            startMenuController.QuitPressed += OnQuitGame;
            pausePopupController.ContinuePressed += OnResume;
            pausePopupController.RestartPressed += OnRestart;
            pausePopupController.QuitPressed += OnQuit;
            gameOverPopupController.PlayAgainPressed += OnRestart;
            gameOverPopupController.QuitPressed += OnQuit;
            playerController.HitCoin += OnHitCoin;
            playerController.HitObstacle += OnHitObstacle;

            gameState = GameState.None;
        }
        private void OnDestroy()
        {
            startMenuController.StartPressed -= OnStartGame;
            startMenuController.QuitPressed -= OnQuitGame;
            pausePopupController.ContinuePressed -= OnResume;
            pausePopupController.RestartPressed -= OnRestart;
            pausePopupController.QuitPressed -= OnQuit;
            gameOverPopupController.PlayAgainPressed -= OnRestart;
            gameOverPopupController.QuitPressed -= OnQuit;
            playerController.HitCoin -= OnHitCoin;
            playerController.HitObstacle -= OnHitObstacle;
        }
        private void Update()
        {
            if(Input.GetKeyDown(KeyCode.Escape) && gameState == GameState.Playing)
            {
                Pause();
            }
            if(gameState == GameState.Playing)
            {
                GetPlayerDistance();
            }
        }
        private void OnHitCoin()
        {
            coin++;
            CoinUpdated?.Invoke(coin);
            CoinHitPlayed?.Invoke();
        }
        private void OnHitObstacle()
        {
            health--;
            HealthUpdated?.Invoke(health);
            ObstacleHitPlayed?.Invoke();

            GameOverCheck();
        }
        private void GetPlayerDistance()
        {
            DistanceUpdated?.Invoke(playerController.transform.position.z);
        }
        private void OnResume()
        {
            pausePopupController.HideCanvas();
            playerController.Activate();
            gameState = GameState.Playing;
        }
        private void OnRestart()
        {
            terrainPoolController.ResetPool();
            OnStartGame();
        }
        private void OnQuit()
        {
            startMenuController.ShowCanvas();
            gameOverPopupController.HideCanvas();
            pausePopupController.HideCanvas();

            gameState = GameState.None;
        }
        private void OnQuitGame()
        {
            Application.Quit();
        }
        private void OnStartGame()
        { 
            health = MaxHealth;
            coin = 0;

            startMenuController.HideCanvas();
            pausePopupController.HideCanvas();
            gameOverPopupController.HideCanvas();
            terrainPoolController.Initialize();
            playerController.ResetState();
            playerController.Activate();

            HealthReseted?.Invoke();
            CoinUpdated?.Invoke(coin);
            DistanceUpdated?.Invoke(playerController.transform.position.z);

            gameState = GameState.Playing;
        }
        private void Pause()
        {
            if(gameState == GameState.Playing)
            {
                pausePopupController.ShowCanvas();
                playerController.Deactivate();
                PausePlayed?.Invoke();
                gameState = GameState.Paused;
            }
            else if(gameState == GameState.Paused)
            {
                OnResume();
            }
        }
        private void GameOverCheck()
        {
            if(health == 0)
            {
                gameOverPopupController.ShowCanvas();
                pausePopupController.HideCanvas();
                GameOverPlayed?.Invoke();
                playerController.Deactivate();
                terrainPoolController.ResetPool();

                gameState = GameState.GameOver;
            }
        }
    }
}
