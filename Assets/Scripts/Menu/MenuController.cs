using UnityEngine;

namespace EndlessRunner.Menu.Controller
{
    public abstract class MenuController : MonoBehaviour
    {
        public Canvas canvas;

        public virtual void ShowCanvas()
        {
            gameObject.SetActive(true);
            canvas.enabled = true;
        }
        public virtual void HideCanvas()
        {
            gameObject.SetActive(false);
            canvas.enabled = false;
        }
    }
}
