using EndlessRunner.Menu.Controller;
using EndlessRunner.UI.Button;
using System;
using UnityEngine;

namespace EndlessRunner.Menu.Start
{
    public class StartMenuController : MenuController
    {
        [SerializeField] private Button startButton;
        [SerializeField] private Button quitButton;

        public event Action StartPressed;
        public event Action QuitPressed;

        private void Start()
        {
            startButton.ButtonReleased += OnStart;
            quitButton.ButtonReleased += OnQuit;
        }
        private void OnDestroy()
        {
            startButton.ButtonReleased -= OnStart;
            quitButton.ButtonReleased -= OnQuit;
        }
        private void OnStart()
        {
            StartPressed?.Invoke();
        }
        private void OnQuit()
        {
            QuitPressed?.Invoke();
        }
    }
}
