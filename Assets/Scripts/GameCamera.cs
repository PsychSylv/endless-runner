using UnityEngine;

namespace EndlessRunner.Camera.Game
{
    public class GameCamera : MonoBehaviour
    {
        [SerializeField] private Transform followedObject;
        [SerializeField] private Vector3 offset;

        private void Update()
        {
            transform.position = new Vector3(offset.x, offset.y, followedObject.position.z + offset.z);
        }
    }
}
