using EndlessRunner.Collectibe.Type;
using UnityEngine;

namespace EndlessRunner.Collectibe.Controller
{
    public class CollectibleController : MonoBehaviour
    {
        [SerializeField] public CollectibleType collectibleType;

        public void HitPlayer()
        {
            gameObject.SetActive(false);
        }
    }
}
    
