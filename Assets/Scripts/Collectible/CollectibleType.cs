namespace EndlessRunner.Collectibe.Type 
{
    public enum CollectibleType
    {
        None,
        Coin,
        Obstacle
    }
}