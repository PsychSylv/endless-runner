using EndlessRunner.UI.Popup.Controller;
using System;
using UnityEngine;

namespace EndlessRunner.UI.Popup.GameOver
{
    public class GameOverPopupController : PopupController
    {
        [SerializeField] private Button.Button restartButton;
        [SerializeField] private Button.Button quitButton;

        public event Action PlayAgainPressed;
        public event Action QuitPressed;

        private void Start()
        {
            restartButton.ButtonReleased += OnPlayAgain;
            quitButton.ButtonReleased += OnQuit;
        }
        private void OnDestroy()
        {
            restartButton.ButtonReleased -= OnPlayAgain;
            quitButton.ButtonReleased -= OnQuit;
        }
        private void OnPlayAgain()
        {
            PlayAgainPressed?.Invoke();
        }
        private void OnQuit()
        {
            QuitPressed?.Invoke();
        }
    }
}
