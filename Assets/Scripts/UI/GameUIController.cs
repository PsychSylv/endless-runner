using EndlessRunner.Manager;
using UnityEngine;
using UnityEngine.UI;

namespace EndlessRunner.UI.Game
{
    public class GameUIController : MonoBehaviour
    {
        [SerializeField] private GameManager gameManager;
        [SerializeField] private Image[] healthImage;
        [SerializeField] private Text coinText;
        [SerializeField] private Text distanceText;

        private void Start()
        {
            gameManager.HealthReseted += OnHealthReseted;
            gameManager.HealthUpdated += OnHealthChanged;
            gameManager.CoinUpdated += OnCoinChanged;
            gameManager.DistanceUpdated += OnDistanceChanged;
        }
        private void OnDestroy()
        {
            gameManager.HealthReseted -= OnHealthReseted;
            gameManager.HealthUpdated -= OnHealthChanged;
            gameManager.CoinUpdated -= OnCoinChanged;
            gameManager.DistanceUpdated -= OnDistanceChanged;
        }
        private void OnHealthReseted()
        {
            for(int i = 0; i < healthImage.Length; i++)
            {
                healthImage[i].enabled = true;
            }
        }
        private void OnHealthChanged(int health)
        {
            healthImage[health--].enabled = false;
        }
        private void OnCoinChanged(int coin)
        {
            coinText.text = coin.ToString();
        }
        private void OnDistanceChanged(float distance)
        {
            distanceText.text = ((int)distance).ToString();
        }
    }
}
