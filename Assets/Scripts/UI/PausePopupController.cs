using EndlessRunner.UI.Popup.Controller;
using System;
using UnityEngine;

namespace EndlessRunner.UI.Popup.Pause
{
    public class PausePopupController : PopupController
    {
        [SerializeField] private Button.Button continueButton;
        [SerializeField] private Button.Button restartButton;
        [SerializeField] private Button.Button quitButton;

        public event Action ContinuePressed;
        public event Action RestartPressed;
        public event Action QuitPressed;

        private void Start()
        {
            continueButton.ButtonReleased += OnContinue;
            restartButton.ButtonReleased += OnRestart;
            quitButton.ButtonReleased += OnQuit;
        }
        private void OnDestroy()
        {
            continueButton.ButtonReleased -= OnContinue;
            restartButton.ButtonReleased -= OnRestart;
            quitButton.ButtonReleased -= OnQuit;
        }
        public void OnContinue()
        {
            ContinuePressed?.Invoke();
        }
        public void OnRestart()
        {
            RestartPressed?.Invoke();
        }
        public void OnQuit()
        {
            QuitPressed?.Invoke();
        }
    }
}
