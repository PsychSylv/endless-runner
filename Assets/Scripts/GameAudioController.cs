using EndlessRunner.Manager;
using UnityEngine;

namespace EndlessRunner.Audio.Controller
{
    public class GameAudioController : MonoBehaviour
    {
        [SerializeField] private GameManager gameManager;
        [SerializeField] private AudioSource audioSource;
        [SerializeField] private AudioClip ballGoalClip;
        [SerializeField] private AudioClip ballHitClip;
        [SerializeField] private AudioClip pauseClip;
        [SerializeField] private AudioClip gameOverClip;

        private void Start()
        {
            gameManager.CoinHitPlayed += OnCoinHit;
            gameManager.ObstacleHitPlayed += OnObstacleHit;
            gameManager.PausePlayed += OnPause;
            gameManager.GameOverPlayed += OnGameOver;
        }
        private void OnDestroy()
        {
            gameManager.CoinHitPlayed -= OnCoinHit;
            gameManager.ObstacleHitPlayed -= OnObstacleHit;
            gameManager.PausePlayed -= OnPause;
            gameManager.GameOverPlayed -= OnGameOver;
        }
        private void OnCoinHit()
        {
            audioSource.PlayOneShot(ballGoalClip);
        }
        private void OnObstacleHit()
        {
            audioSource.PlayOneShot(ballHitClip);
        }
        private void OnPause()
        {
            audioSource.PlayOneShot(pauseClip);
        }
        private void OnGameOver()
        {
            audioSource.PlayOneShot(gameOverClip);
        }
    }
}
