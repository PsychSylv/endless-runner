using EndlessRunner.Terrain.Controller;
using UnityEngine;

namespace EndlessRunner.Terrain.Pool
{
    public class TerrainPoolController : MonoBehaviour
    {
        [SerializeField] private TerrainPieceController[] terrainPieces;
        [SerializeField] private TerrainPieceController terrain;
        [SerializeField] private Transform terrainHolder;
        [SerializeField] private int poolValue;
        private float minimumZ = -10f;
        private float currentZ = -10f;
        private const float OffsetZ = 2f;

        private void Start()
        {
            terrainPieces = new TerrainPieceController[poolValue];
        }

        public void Initialize()
        {
            currentZ = 0;
            for(int i = 0; i < poolValue; i++)
            {
                terrainPieces[i] = Instantiate(terrain, Vector3.zero, Quaternion.identity, terrainHolder);
                terrainPieces[i].gameObject.transform.position = new Vector3(0, 0, currentZ += OffsetZ);
            }
        }
        public void GetPooledTerrain()
        {
            for(int i = 0; i < terrainPieces.Length; i++)
            {
                if(!terrainPieces[i].gameObject.activeInHierarchy)
                {
                    terrainPieces[i].gameObject.transform.position = new Vector3(0, 0, currentZ += OffsetZ);
                    terrainPieces[i].RandomizeContent();
                    terrainPieces[i].gameObject.SetActive(true);
                }
            }
        }
        public void ResetPool()
        {
            currentZ = minimumZ;

            for(int i = 0; i < terrainPieces.Length; i++)
            {
                terrainPieces[i].gameObject.SetActive(false);
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.gameObject.CompareTag("Terrain"))
            {
                other.gameObject.GetComponent<TerrainPieceController>().HideContent();
                GetPooledTerrain();
            }
        }
    }
}
