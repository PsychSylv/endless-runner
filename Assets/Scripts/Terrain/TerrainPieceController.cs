using UnityEngine;

namespace EndlessRunner.Terrain.Controller
{
    public class TerrainPieceController : MonoBehaviour
    {
        [SerializeField] private GameObject[] coin;
        [SerializeField] private GameObject[] obstacle;

        private GameObject currentGameObject;
        private const int MinObject = 0;
        private const int MaxObject = 3;
        private int random;
        private int gameObjectIndex;

        public void RandomizeContent()
        {
            random = Random.Range(MinObject, MaxObject);

            if (random == 0)
            {
                gameObjectIndex = Random.Range(0, coin.Length);
                currentGameObject = coin[gameObjectIndex];
                ShowObject(currentGameObject);
            }
            else if (random == 1)
            {
                gameObjectIndex = Random.Range(0, obstacle.Length);
                currentGameObject = obstacle[gameObjectIndex];
                ShowObject(currentGameObject);
            }
            else
            {
                return;
            }
        }
        public void HideContent()
        {
            gameObject.SetActive(false);
            
            if (currentGameObject != null)
            {
                currentGameObject.SetActive(false);
            }
        }
        private void ShowObject(GameObject gameObject)
        {
            gameObject.SetActive(true);
        }
    }
}
