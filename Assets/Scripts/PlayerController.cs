using EndlessRunner.Collectibe.Controller;
using System;
using UnityEngine;

namespace EndlessRunner.Player.Controller
{
    public class PlayerController : MonoBehaviour
    {
        [SerializeField] private Vector3 movespeed;
        [SerializeField] private Vector3 horizontalMovespeed;
        [SerializeField] private Vector3 initialPosition;
        private Vector3 targetPosition;
        private int currentLane = 1;
        private float currentX;
        private bool canMove = false;
        private bool canChangeLane = true;

        private const int MinLane = 0;
        private const int DefaultLane = 1;
        private const int MaxLane = 2;
        private const float MinimumOffsetX = 0.05f;
        private const float OffsetX = 0.5f;

        public event Action HitCoin;
        public event Action HitObstacle;

        public void ResetState()
        {
            currentLane = DefaultLane;
            transform.position = targetPosition = initialPosition;
        }
        public void Activate()
        {
            canMove = true;
        }
        public void Deactivate()
        {
            canMove = false;
        }
        private void Update()
        {
            if(canMove)
            {
                Move();
                if(canChangeLane)
                {
                    ChangeLane();
                }
            }
        }
        private void Move()
        {
            transform.Translate(movespeed);
            
            if (transform.position.x - targetPosition.x > MinimumOffsetX || transform.position.x - targetPosition.x < -MinimumOffsetX)
            {
                currentX += ((targetPosition.x - transform.position.x) * horizontalMovespeed.x * Time.deltaTime);
                transform.position = new Vector3(currentX, transform.position.y, transform.position.z);
            }
            else
            {
                transform.position = new Vector3(targetPosition.x, transform.position.y, transform.position.z);
                canChangeLane = true;
            }
        }
        private void ChangeLane()
        {
            if ((Input.GetKeyDown(KeyCode.A) || (Input.GetMouseButtonDown(0) && Input.mousePosition.x < Screen.width / 2)) && currentLane != MinLane)
            {
                currentLane--;
                targetPosition.x = transform.position.x - OffsetX;
                canChangeLane = false;
            }
            else if ((Input.GetKeyDown(KeyCode.D) || (Input.GetMouseButtonDown(0) && Input.mousePosition.x > Screen.width / 2)) && currentLane != MaxLane)
            {
                currentLane++;
                targetPosition.x = transform.position.x + OffsetX;
                canChangeLane = false;
            }
        }
        
        private void OnTriggerEnter(Collider other)
        {
            if(other.CompareTag("Coin"))
            {
                other.GetComponent<CollectibleController>().HitPlayer();
                HitCoin?.Invoke();
            }
            else if(other.CompareTag("Obstacle"))
            {
                other.GetComponent<CollectibleController>().HitPlayer();
                HitObstacle?.Invoke();
            }
        }
    }
}