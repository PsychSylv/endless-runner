using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EndlessRunner.Game.State
{
    public enum GameState
    {
        None,
        Playing,
        Paused,
        GameOver
    }
}
